# Changelog

## [0.0.0] 2023/4/9

- Clear and standarize code.
- Include empty index.php to folders.
- Add version number support to Plugin class.
- Setup getters for version and plugin name.


