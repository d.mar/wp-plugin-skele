<?php
namespace Plugin;

use Plugin\classes\Loader;

defined('ABSPATH') OR exit;

class Plugin
{
    public $name = 'plugin-name';
    public $version = PLUGIN_VERSION;
    private $loader;
    public function __construct()
    {
        $this->loadDependencies();
        $this->setLocale();
    }
    public function getName()
    {
        return $this->name;
    }
    public function getVersion()
    {
        return $this->version;
    }
    public function init()
    {
        $this->loader->run();
    }
    private function loadDependencies()
    {
        $this->loader = new Loader();
    }
    private function setLocale() { return; }
    public static function onActivation() { }
    public static function onDeactivation() { }
    public static function onUninstall() { }
}
