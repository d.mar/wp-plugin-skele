<?php defined('ABSPATH') OR exit;
/**
 * Plugin Name:         wp-plugin-skele
 * Description:         Skeleton for Wordpress plugins.
 * Version:             0.1.0-dev
 * Requires at least:   5.8
 * Requires PHP:        8.2
 * Author:              David Martin
 * Author URI:          https://dmar.dev
 * Plugin URI:
 * License:             GNU GENERAL PUBLIC LICENSE
 * Text Domain:         wp-plugin-skele
 * Domain Path:         /languages
 */

// ----------------------------------------------------------------------------
// Plugin Bootstrap
// ----------------------------------------------------------------------------

define('PLUGIN_MIN_PHP_VER', '8.1');
define('PLUGIN_VERSION', '1.0.0');
define('PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url(__FILE__));
define('PLUGIN_ADM_MAIL', '');

require_once PLUGIN_PATH . 'vendor/autoload.php';
require_once PLUGIN_PATH . 'Plugin/Plugin.php';

register_activation_hook(
    __FILE__,
    array('\Plugin\Plugin', 'onActivation')
);
register_deactivation_hook(
    __FILE__,
    array('\Plugin\Plugin', 'onDeactivation')
);
register_uninstall_hook(
    __FILE__,
    array('\Plugin\Plugin', 'onUninstall')
);

$plugin = new \Plugin\Plugin();
$plugin->init();

